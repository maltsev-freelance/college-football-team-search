# College football team search

## Overview
You have been provided a comma separated value (.csv) file. This data set should remain unedited. Your mission is as follows:
Create a single web page in asp.net C# with a textbox and a button that, when clicked, searches a given dataset and returns the entire record of results on screen
Include a drop down that defaults to all columns but can be selected to search only one column
Provide error checking where appropriate
If you have the talent and time, you may style the page.

Additional Requests:
Please don’t use a Database
You MUST include a write-up of how you decided to go about completing this project
Your project should work on any developer’s machine when turned in as long as they have Visual Studio as a web application.
Focus on how a user would use the application

## Sequence

1. Create Asp.Net Core web-application with MVC+WebAPI template.
2. Removed unnessesary views, controllers and scripts, leave only HomeController, Index web-view.
3. Added npm-package file `packages.json` with `Gulp` in dev. dependency and `jQuery`+`Popper.Js`+`Bootstrap` in regular dependenticies.
4. Created `gulpfile.js` with script to transfer files from `dist`-folders of required packages in `node_modules` inside `wwwroot\vendor` folder, and referenced those package in view `Views/Shared/_Layout.cshtml`. Connect `gulpfile` with `Before build event` in Visual Studio Task Runner, so source will be updated before every build.
5. Added `Models/SearchRequestModel.cs` with two filters of the request. 
6. Added `Models/DatasetItem.cs` with all properties presented in CSV-file of dataset.
7. Added eunm `FilterFieldType.cs` with all field-filter variants.
8. Added Interface from file `Servicies/IDataSearch.cs` with single method `Find`. 
9. Created empty implementation of `IDataSearch` named `DatasetCachedSearch`.
10. Added dataset into `Dataset/CollegeFootballTeamWinsWithMascots.csv` in it it's property `Copy to ouput directory` select `Copy always`.
11. In file `Startup.cs` in method `ConfigureServices` register singleton service:
```
services.AddSingleton<IDataSearch>(new SimilarityDatasetSearch("Dataset/CollegeFootballTeamWinsWithMascots.csv"));
```
12. In `DatasetCachedSearch` implemented reading of dataset in `ReadDataset`, create VIRTUAL protected methods for finding in all fields (`FindInAllFields`) and in specific field (`FindInField`).
13. Create a switch-case in method `Find`.
14. Creape api-controller in `Api/SearchController.cs` with only 1 `POST` method and `IDataSearch` parameter in constructor, so registred service could be passed here.
15. In view `Home/Index` created basic markup for filters and resulted table. In `site.css` deleted all view's styles and added styles for spinner.
16. In `site.js` added function `search` to send POST-request to `api/search` and also methods to display spinner and table of results.
17. In `site.js` added methods to draw table of results via jQuery (methods `drawTable` and `drawRow`).
18. Check the site, it should be working.
19. Added Nuget-package `F23.StringSimilarity`.
20. Added `Servicies/SimilarityDatasetSearch.cs` which inheritated `DatasetCachedSearch`. Added consturctor with `filename` and `similaryThreshold` (set it to 0.7).
21. Overrided method `FindInField` to use N-Gram for finding distance between strings and use it in LINQ predicate for diltering records.  