﻿/// <binding BeforeBuild='scripts' />
var gulp = require('gulp');
var merge = require('merge-stream');

// Dependency Dirs
var deps = {
    "bootstrap": {
        "dist/**/*": ""
    },
    "jquery": {
        "dist/*": ""
    },
    "popper.js": {
        "dist/**/*": ""
    }
};

// This task is used to copy all needed js-libs from node_modules to "wwwroot/vendor/ folder.
gulp.task("scripts", function () {

    var streams = [];

    for (var prop in deps) {
        console.log("Prepping Scripts for: " + prop);
        for (var itemProp in deps[prop]) {
            streams.push(gulp.src("node_modules/" + prop + "/" + itemProp)
                .pipe(gulp.dest("wwwroot/vendor/" + prop + "/" + deps[prop][itemProp])));
        }
    }

    return merge(streams);

});