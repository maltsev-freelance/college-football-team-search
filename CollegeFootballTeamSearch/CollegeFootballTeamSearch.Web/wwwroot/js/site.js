﻿"use strict";

function search() {
    displayTable(false);
    toggleLoading(true);    
    var request = {
        query: $("#query").val(),
        fieldType: $("#field :selected").val()
    };

    var headers = new Headers();
    headers.append("Content-Type", "application/json");

    fetch("/api/search",
            {
                method: "POST",
                headers: headers,
                body: JSON.stringify(request)
            })
        .then(function(response) {
            return response.json();
        })
        .then(function(results) {
            drawTable(results);
            toggleLoading(false);
            displayTable(true);
        });
}

function toggleLoading(isLoading) {
    if (isLoading) {
        $("#loader").show();
    } else {
        $("#loader").hide();
    }
}

function displayTable(isShown) {
    if (isShown) {
        $("#results").show();
    } else {
        $("#results").hide();
    }
}

function drawTable(data) {
    $("#results tbody").empty();
    for (var i = 0; i < data.length; i++) {
        drawRow(data[i]);
    }
}

function drawRow(rowData) {
    var row = $("<tr />");
    
    $("#results tbody").append(row);
    row.append($("<td>" + rowData.rank + "</td>"));
    row.append($("<td>" + rowData.team + "</td>"));
    row.append($("<td>" + rowData.mascot + "</td>"));
    row.append($("<td>" + rowData.dateOfLastWin + "</td>"));
    row.append($("<td>" + rowData.winningPercetnage + "</td>"));
    row.append($("<td>" + rowData.wins + "</td>"));
    row.append($("<td>" + rowData.losses + "</td>"));
    row.append($("<td>" + rowData.ties + "</td>"));
    row.append($("<td>" + rowData.games + "</td>"));
}

$("#filters").submit(function(event) {
    event.preventDefault();
    search();
    return false;
});


displayTable(false);
toggleLoading(false);  
