﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollegeFootballTeamSearch.Web.Enums
{
    public enum FilterFieldType
    {
        None = 0,
        Rank = 1,
        Team = 2,
        Mascot = 4,
        DateOfLastWin = 8,
        WinningPercetnage = 16,
        Wins = 32,
        Losses = 64,
        Ties = 128,
        Games = 256,

        All = Rank & Team & Mascot & DateOfLastWin & WinningPercetnage & Wins & Losses & Ties & Games
    }
}
