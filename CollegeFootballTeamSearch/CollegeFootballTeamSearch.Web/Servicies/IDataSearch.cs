﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CollegeFootballTeamSearch.Web.Enums;
using CollegeFootballTeamSearch.Web.Models;

namespace CollegeFootballTeamSearch.Web.Servicies
{
    public interface IDataSearch
    {
        List<DatasetItem> Find(string text, FilterFieldType field);
    }
}
