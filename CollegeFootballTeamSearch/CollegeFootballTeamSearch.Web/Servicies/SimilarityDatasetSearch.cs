﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CollegeFootballTeamSearch.Web.Models;
using F23.StringSimilarity;

namespace CollegeFootballTeamSearch.Web.Servicies
{
    public class SimilarityDatasetSearch: DatasetCachedSearch
    {
        private readonly double _similaryThreshold;

        public SimilarityDatasetSearch(string filename, double similaryThreshold = 0.7) : base(filename)
        {
            _similaryThreshold = similaryThreshold;
        }

        protected override List<DatasetItem> FindInField(string text, Func<DatasetItem, string> fieldRetrievingFunc)
        {
            var fieldsValues =
                CachedDataset.Select(tuple => Tuple.Create(fieldRetrievingFunc(tuple.Item2), tuple.Item2));

            var gram = new NGram(3);
            var normalizedText = text.ToLower().Trim();
            return fieldsValues.Where(tuple => gram.Distance(tuple.Item1.ToLower(), normalizedText) <= _similaryThreshold)
                .Select(tuple => tuple.Item2)
                .ToList();
        }
    }
}
