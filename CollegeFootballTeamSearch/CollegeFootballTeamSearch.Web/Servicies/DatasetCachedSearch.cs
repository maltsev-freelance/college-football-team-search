﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CollegeFootballTeamSearch.Web.Enums;
using CollegeFootballTeamSearch.Web.Models;

namespace CollegeFootballTeamSearch.Web.Servicies
{
    public class DatasetCachedSearch: IDataSearch
    {
        protected readonly List<Tuple<string, DatasetItem>> CachedDataset;

        public DatasetCachedSearch(string filename)
        {
            CachedDataset = ReadDataset(filename);
        }
        public virtual List<DatasetItem> Find(string text, FilterFieldType field)
        {
            switch (field)
            {
                case FilterFieldType.All:
                    return FindInAllFields(text);
                case FilterFieldType.Rank:
                    return FindInField(text, item => item.Rank);
                case FilterFieldType.Team:
                    return FindInField(text, item => item.Team);
                case FilterFieldType.Mascot:
                    return FindInField(text, item => item.Mascot);
                case FilterFieldType.DateOfLastWin:
                    return FindInField(text, item => item.DateOfLastWin);
                case FilterFieldType.WinningPercetnage:
                    return FindInField(text, item => item.WinningPercetnage);
                case FilterFieldType.Wins:
                    return FindInField(text, item => item.Wins);
                case FilterFieldType.Losses:
                    return FindInField(text, item => item.Losses);
                case FilterFieldType.Ties:
                    return FindInField(text, item => item.Ties);
                case FilterFieldType.Games:
                    return FindInField(text, item => item.Games);
                default:
                    throw new Exception(
                        $"Could not get find data for field {Enum.GetName(typeof(FilterFieldType), field)}");
            }
        }

        private List<Tuple<string, DatasetItem>> ReadDataset(string filename)
        {
            return File.ReadAllLines(filename)
                .Skip(1)
                .Select(s => s.Split(","))
                .Select(strings => new DatasetItem()
                {
                    Rank = strings[0].Trim(),
                    Team = strings[1].Trim(),
                    Mascot = strings[2].Trim(),
                    DateOfLastWin = strings[3].Trim(),
                    WinningPercetnage = $@"{Math.Round(
                        double.TryParse(strings[4].Trim(), NumberStyles.AllowDecimalPoint,
                            CultureInfo.InvariantCulture, out double percent)
                            ? percent
                            : 0,
                        4):P}",
                    Wins = strings[5].Trim(),
                    Losses = strings[6].Trim(),
                    Ties = strings[7].Trim(),
                    Games = strings[8].Trim()
                })
                .Select(item => Tuple.Create(item.ToString(), item))
                .ToList();
        }

        protected virtual List<DatasetItem> FindInAllFields(string text)
        {
            return CachedDataset.Where(tuple =>
                    text.Split(" ")
                        .All(s => tuple.Item1.IndexOf(s, StringComparison.InvariantCultureIgnoreCase) >= 0))
                .Select(tuple => tuple.Item2)
                .ToList();
        }

        protected virtual List<DatasetItem> FindInField(string text, Func<DatasetItem, string> fieldRetrievingFunc)
        {
            var fieldsValues =
                CachedDataset.Select(tuple => Tuple.Create(fieldRetrievingFunc(tuple.Item2), tuple.Item2));

            return fieldsValues.Where(tuple => tuple.Item1.IndexOf(text, StringComparison.InvariantCultureIgnoreCase) >= 0)
                .Select(tuple => tuple.Item2)
                .ToList();
        }
    }
}
