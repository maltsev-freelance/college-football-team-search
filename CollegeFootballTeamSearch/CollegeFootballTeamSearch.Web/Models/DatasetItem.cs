﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollegeFootballTeamSearch.Web.Models
{
    public class DatasetItem
    {
        public string Rank { get; set; }
        public string Team { get; set; }
        public string Mascot { get; set; }
        public string DateOfLastWin { get; set; }
        public string WinningPercetnage { get; set; }
        public string Wins { get; set; }
        public string Losses { get; set; }
        public string Ties { get; set; }
        public string Games { get; set; }

        public override string ToString()
        {
            return string.Join(" ", Rank, Team, Mascot, DateOfLastWin, WinningPercetnage, Wins, Losses, Ties, Games);
        }
    }
}
