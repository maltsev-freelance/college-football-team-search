﻿using CollegeFootballTeamSearch.Web.Enums;

namespace CollegeFootballTeamSearch.Web.Models
{
    public class SearchRequestModel
    {
        public string Query { get; set; }

        public FilterFieldType FieldType { get; set; }
    }
}
