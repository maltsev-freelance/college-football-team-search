﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CollegeFootballTeamSearch.Web.Models;
using CollegeFootballTeamSearch.Web.Servicies;
using Microsoft.AspNetCore.Mvc;

namespace CollegeFootballTeamSearch.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class SearchController : Controller
    {
        private readonly IDataSearch _dataSearch;

        public SearchController(IDataSearch dataSearch)
        {
            _dataSearch = dataSearch;
        }

        [HttpPost]
        public IEnumerable<DatasetItem> Post([FromBody]SearchRequestModel model)
        {
            return _dataSearch.Find(model.Query, model.FieldType);
        }
    }
}
